import React from "react";
import MainLayout from "../layouts/MainLayout";
import ProductList from "../components/ProductList";
import {products} from "../mock";

const IndexPage = () => (
    <div>
        <MainLayout>
            <h1>Shop page</h1>
            <p>List of products: </p>
            <ProductList products={products}/>
        </MainLayout>
    </div>
);

export default IndexPage;
