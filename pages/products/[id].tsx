import React, {useEffect, useState} from 'react';
import MainLayout from "../../layouts/MainLayout";
import {useRouter} from "next/router";
import {products} from "../../mock";
import {ICart} from "../../types/product";
import p from "../../styles/prodPage.module.css"

const ProductPage = () => {
    const [quantity, setQuantity] = useState(1);
    const router = useRouter();
    const product = products[Number(router.query.id)-1]
    console.log(product)
    const addToCart = () => {
        const cartInfo = JSON.parse(localStorage.getItem('cartInfo') || '[]') as ICart[];
        const existProductIdx: number = cartInfo.findIndex(c => c.product.id === product.id);
        if (existProductIdx !== -1) {
            cartInfo[existProductIdx].count+=quantity;
        } else {
            cartInfo.push({
                count: quantity,
                product
            })
        }
        localStorage.setItem('cartInfo', JSON.stringify(cartInfo))
        router.push('/cart')
    }
    const incQuantity = () => {
        setQuantity(quantity+1)
    }
    const decQuantity = () => {
        if(quantity>1) {
            setQuantity(quantity-1);
        }
    }
    return (
        <MainLayout>
            <div className={p.container}>
                <div className={p.imgs}>
                    <div className={p.img}>IMG</div>
                    <div className={p.img}>IMG</div>
                </div>
                <div className={p.prodInfo}>
                    <h1>{product?.name}</h1>
                    <h3>${product?.price}</h3>
                    <h3>Quantity</h3>
                    <div className={p.quantity}>
                        <button onClick={decQuantity}>-</button>
                        <span>{quantity}</span>
                        <button onClick={incQuantity}>+</button>
                    </div>
                    <h3>Overall price: {quantity*product?.price}$</h3>
                    <button className={p.addToCart} onClick={addToCart}>ADD TO CART</button>

                    <h3>Description</h3>
                    <p>A solar panel is actually a collection of solar (or photovoltaic) cells, which can be used to generate electricity through photovoltaic effect. These cells are arranged in a grid-like pattern on the surface of solar panels. Includes: Lithium Battery 4600Wt power Unlimited time of use</p>

                    <h3>Includes:</h3>
                    <ul>
                        <li>Lithium Battery</li>
                        <li>4600Wt power</li>
                        <li>Unlimited time of use</li>
                    </ul>
                </div>
            </div>
        </MainLayout>
    );
};
export default ProductPage;
